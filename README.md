# MOVIES

## CAPAS DE LA APLICACION

### DATA
Esta capa se encarga de las gestion toda la informacio que guarda en la base 
de datos o en cache

1.  La clase MoviesDatabse es donde se crea y se configura la base de datos
2.  **Package Dao:** En este paquete crearon las interfaces que se encargan de gestionar la informacion que se guarda en base datos
3.  **Package Entities:** En este paquete se crearon las tablas y modelo de la base de datos
  
### NETWORK
Esta capa se encarga  de la comunicacion a internet en este caso para las peticiones al api

1.  **Package Client:** Aqui se configura el client que va ser las peticiones al api en este caso Retrofit
2.  **ServiceConstants:** En esta clase colocamos todo los string que se utilizan en esta capa, como lo son:
    *  Apikey del api
    *  Url del api
3.  **Package Response:** Son las entidades de las respuestas del api
4.  MoviesService es la clase donde se configuran los metodos que se consumen del api

### VIWES
Esta capa se encarga de las gestion de las vistas de la aplicacion 

1. PopularFragment: MEs la que muestra la vista con los datos que se le procesa
2. PopularMovieViewModel: Se encarga de gestionar la informacion que se procesa a la vista
3. TopRatedFragment: Es la que muestra la vista con los datos que se le procesa
4. TopRatedMovieViewModel: Se encarga de gestionar la informacion que se procesa a la vista
5. UpcomingFragment: Es la que muestra la vista con los datos que se le procesa 
6. UpcomingMovieViewModel: Se encarga de gestionar la informacion que se procesa a la vista
7. DetailMovie: Es un actity que muestra los datos de un pelicula
8. DetailMovieViewModel: Se encarga de procesar la informacion dependiendo de la pelicula
9. MainNavigationActivity: Es el activity donde se configura el bottom Navigation View.

### BUSINESS
Esta capa se encarga de tener toda la logica de negocio y en un punto entre la capa de datos y la vista

1.  GenreRepository: Es la que contiene toda la logica de los generos de las peliculas
2.  MoviesRepository: Es el que tiene la logica de las peliculas, tanto como para Popular, TopRated y Upcoming

##En qué consiste el principio de responsabilidad única? Cuál es su propósito?
Es cuando un objecto realiza una unica cosa, con lo cual se he mas flexible, sin tener dependencias de otras clases y los cambios sean transparentes

##Qué características tiene, según su opinión, un “buen” código o código limpio?

*  Con patrones de diseño
*  Sin codigo hardcodeado
*  Facil de entender en otras palabras que siga los principio KISS(Keep It Simple, Stupid) ya que todo funciona mejor si se mantienen simple
*  Nomeclatura estanderizadas
*  Comentarios en el codigo
*  En un solo idioma, ya que muchas veces ocupamos el spanglish 
*  Seguir las recomendaciones o sguerencias de los componentes del autor.
