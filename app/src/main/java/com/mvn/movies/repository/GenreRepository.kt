package com.mvn.movies.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.mvn.movies.data.room.MoviesDatabase
import com.mvn.movies.data.room.dao.GenreDao
import com.mvn.movies.data.room.entities.Genre
import com.mvn.movies.data.room.entities.Popular
import com.mvn.movies.network.client.RestClientMovies
import com.mvn.movies.network.constants.ServiceConstants
import com.mvn.movies.network.responses.GenreResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenreRepository(application: Application) {

    private val genreDao: GenreDao

    init {
        val database = MoviesDatabase.getDatabase(application)
        genreDao = database.genreDao()
    }


    fun getAllGenres() :LiveData<List<Genre>>{
        RestClientMovies.getAllGenre(ServiceConstants.API_KEY_VALUE).enqueue(object :
                Callback<GenreResponse> {

            override fun onResponse(call: Call<GenreResponse>, response: Response<GenreResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    response.body()!!.genres!!.forEach {
                        updateInsertGenre(it)
                    }
                }
            }

            override fun onFailure(call: Call<GenreResponse>, t: Throwable) {
            }
        })

        return genreDao.getAllGenre()
    }


    fun getGenresByPopularId(filterValues: Long): LiveData<List<Genre>> {
        return genreDao.getAllGenreByPopularId(filterValues)
    }

    private fun updateInsertGenre(genre: Genre) {
        AsyncTask.execute {
            val id = genreDao.insertGenre(genre)
            if (id == -1L) {
                genreDao.updateGenre(genre)
            }
        }
    }


}