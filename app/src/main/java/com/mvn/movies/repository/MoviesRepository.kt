package com.mvn.movies.repository

import android.app.Application
import android.database.sqlite.SQLiteConstraintException
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.mvn.movies.data.room.MoviesDatabase
import com.mvn.movies.data.room.dao.*
import com.mvn.movies.data.room.entities.*
import com.mvn.movies.network.responses.PopularMoviesResponse
import com.mvn.movies.network.client.RestClientMovies
import com.mvn.movies.network.constants.ServiceConstants
import com.mvn.movies.network.responses.TopRatedMoviesResponse
import com.mvn.movies.network.responses.UpcomingMoviesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MoviesRepository(application: Application) {

    private val popularDao: PopularDao
    private val popularGenreDao: PopularGenreDao
    private val topRatedDao: TopRatedDao
    private val topRatedGenreDao: TopRatedGenreDao
    private val upcomingDao: UpcomingDao
    private val upcomingGenreDao: UpcomingGenreDao

    init {
        val database =  MoviesDatabase.getDatabase(application)
        popularDao = database.popularDao()
        popularGenreDao = database.popularGenreDao()
        topRatedDao = database.topRatedDao()
        topRatedGenreDao = database.topRatedGenreDao()
        upcomingDao = database.upcomingDao()
        upcomingGenreDao = database.upcomingGenreDao()
    }

    //region Popular Movies

    fun getAllPopularMovies(): DataSource.Factory<Int, Popular> = popularDao.getAllPopularsMovies()

    fun getPopulars(page:Int){
        RestClientMovies.getAllPopularMovies(ServiceConstants.API_KEY_VALUE, page).enqueue(object : Callback<PopularMoviesResponse> {

            override fun onResponse(call: Call<PopularMoviesResponse>, response: Response<PopularMoviesResponse>) {
                if (response.isSuccessful) {
                    updateInsertPopularMovie(response.body()!!.results!!)
                }
            }

            override fun onFailure(call: Call<PopularMoviesResponse>, t: Throwable) {
            }
        })
    }

    fun getPopularMoviesByGenre(filterValues: List<Int>): DataSource.Factory<Int, Popular>  = popularDao.getAllPopularMoviesByGenre(filterValues)

    private fun updateInsertPopularMovie(populars: List<Popular>) {
        AsyncTask.execute {
            populars.forEach {popular ->
                val id = popularDao.insertPopularMovie(popular)
                if (id == -1L) {
                    popularDao.updatePopularMovie(popular)
                }

                popular.genres.forEach{
                    val popularGenre = PopularGenre(it, popular.id)
                    try {
                        val popularGenreId = popularGenreDao.insertPopularGenre(popularGenre)
                        if (popularGenreId == -1L) {
                            popularGenreDao.updatePopularGenre(popularGenre)
                        }
                    } catch (ex: SQLiteConstraintException) {
                        ex.toString()
                    }
                }
            }
        }
    }

    fun deleteAllPopularsMovies() {
        popularDao.deleteAllPopularMovies()
    }

    fun getPopularMovie(movieId: Long): LiveData<Popular> {
        return popularDao.getPopularMovie(movieId)
    }

    //endregion

    //region Top Rated Movies

    fun getAllTopRatedMovies(): DataSource.Factory<Int, TopRated> = topRatedDao.getAllTopRatedMovies()

    fun getTopRates(page:Int){
        RestClientMovies.getAllTopRatedMovies(ServiceConstants.API_KEY_VALUE, page).enqueue(object : Callback<TopRatedMoviesResponse> {

            override fun onResponse(call: Call<TopRatedMoviesResponse>, response: Response<TopRatedMoviesResponse>) {
                if (response.isSuccessful) {
                    updateInsertTopRatedMovie(response.body()!!.results!!)
                }
            }

            override fun onFailure(call: Call<TopRatedMoviesResponse>, t: Throwable) {
            }
        })
    }

    fun getTopRatedMoviesByGenre(filterValues: List<Int>): DataSource.Factory<Int, TopRated>  = topRatedDao.getAllTopRatedMoviesByGenre(filterValues)

    private fun updateInsertTopRatedMovie(topRates: List<TopRated>) {
        AsyncTask.execute {
            topRates.forEach {topRated ->
                val id = topRatedDao.insertTopRatedMovie(topRated)
                if (id == -1L) {
                    topRatedDao.updateTopRatedMovie(topRated)
                }

                topRated.genres.forEach{
                    val topRatedGenre = TopRatedGenre(it, topRated.id)
                    try {
                        val popularGenreId = topRatedGenreDao.insertTopRatedGenre(topRatedGenre)
                        if (popularGenreId == -1L) {
                            topRatedGenreDao.updateTopRatedGenre(topRatedGenre)
                        }
                    } catch (ex: SQLiteConstraintException) {
                        ex.toString()
                    }
                }
            }
        }
    }

    fun deleteAllTopRatesMovies() {
        topRatedDao.deleteAllTopRatedMovies()
    }

    fun getTopRateMovie(movieId: Long): LiveData<TopRated> {
        return topRatedDao.getTopRatedMovie(movieId)
    }

    //endregion

    //region Upcoming Movies

    fun getAllUpcomingMovies(): DataSource.Factory<Int, Upcoming> = upcomingDao.getAllUpcomingMovies()

    fun getUpcoming(page:Int){
        RestClientMovies.getAllUpcomingMovies(ServiceConstants.API_KEY_VALUE, page).enqueue(object : Callback<UpcomingMoviesResponse> {

            override fun onResponse(call: Call<UpcomingMoviesResponse>, response: Response<UpcomingMoviesResponse>) {
                if (response.isSuccessful) {
                    updateInsertUpcomingMovie(response.body()!!.results!!)
                }
            }

            override fun onFailure(call: Call<UpcomingMoviesResponse>, t: Throwable) {
            }
        })
    }

    fun getUpcomingMoviesByGenre(filterValues: List<Int>): DataSource.Factory<Int, Upcoming>  = upcomingDao.getAllUpcomingMoviesByGenre(filterValues)

    private fun updateInsertUpcomingMovie(upcomings: List<Upcoming>) {
        AsyncTask.execute {
            upcomings.forEach {upcoming ->
                val id = upcomingDao.insertUpcomingMovie(upcoming)
                if (id == -1L) {
                    upcomingDao.updateUpcomingMovie(upcoming)
                }

                upcoming.genres.forEach{
                    val upcomingGenre = UpcomingGenre(it, upcoming.id)
                    try {
                        val upcomingGenreId = upcomingGenreDao.insertUpcomingGenre(upcomingGenre)
                        if (upcomingGenreId == -1L) {
                            upcomingGenreDao.updateUpcomingGenre(upcomingGenre)
                        }
                    } catch (ex: SQLiteConstraintException) {
                        ex.toString()
                    }
                }
            }
        }
    }

    fun deleteAllUpcomingMovies() {
        upcomingDao.deleteAllUpcomingMovies()
    }

    fun getUpcomingMovie(movieId: Long): LiveData<Upcoming> {
        return upcomingDao.getUpcomingMovie(movieId)
    }

    //endregion

}
