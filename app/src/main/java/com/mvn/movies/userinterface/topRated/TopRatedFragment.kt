package com.mvn.movies.userinterface.topRated

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mvn.movies.R
import com.mvn.movies.model.TopRatedViewModel
import com.mvn.movies.presenter.adapter.TopRatedAdapter
import com.mvn.movies.userinterface.base.BaseFragment
import com.mvn.movies.utils.ChipUtils
import kotlinx.android.synthetic.main.fragment_top_rated.*


class TopRatedFragment : BaseFragment() {

    /*VIEW MODELS*/
    private lateinit var topRatedViewModel: TopRatedViewModel

    /*VARIABLE*/
    private lateinit var topRatedAdapter: TopRatedAdapter

    companion object {
        @JvmStatic
        fun newInstance() = TopRatedFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_top_rated, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initializeViews()
    }

    override fun initializeViews() {
        initializeComponents()
    }

    override fun initializeComponents() {

        activity!!.title = getString(R.string.top_rated_movies)
        topRatedAdapter = TopRatedAdapter(context!!)
        recycler.adapter = topRatedAdapter
        chipGroupGenres.visibility = View.GONE

        refresh.setOnRefreshListener {
            topRatedViewModel.refresh()
        }

        initializeViewModel()
    }

    private fun initializeViewModel() {

        topRatedViewModel = ViewModelProviders.of(this).get(TopRatedViewModel::class.java)
        topRatedViewModel.getTopRatesMovies.observe(this, Observer { data ->
            topRatedAdapter.submitList(data)
            refresh.isRefreshing = false
        })

        topRatedViewModel.getGenres.observe(this, Observer { data ->

            chipGroupGenres.removeAllViews()
            if (context != null) {
                data.forEach { genre ->
                    val chip = ChipUtils.createChip(genre.name!!, true, context!!)
                    chip.id = genre.id.toInt()
                    chip.setOnCheckedChangeListener { buttonView, isChecked ->
                    }
                    chipGroupGenres.addView(chip)
                }
            }
            chipGroupGenres.setChipSpacing(10)
        })
    }

}
