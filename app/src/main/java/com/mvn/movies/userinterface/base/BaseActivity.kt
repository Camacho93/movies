package com.mvn.movies.userinterface.base


import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    abstract fun initializeViews()
    abstract fun initializeComponents()
}