package com.mvn.movies.userinterface.upcoming

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mvn.movies.R
import com.mvn.movies.model.UpcomingViewModel
import com.mvn.movies.presenter.adapter.UpcomingAdapter
import com.mvn.movies.userinterface.base.BaseFragment
import com.mvn.movies.utils.ChipUtils
import kotlinx.android.synthetic.main.fragment_top_rated.*


class UpcomingFragment : BaseFragment() {

    /*VIEW MODELS*/
    private lateinit var upcomingViewModel: UpcomingViewModel

    /*VARIABLE*/
    private lateinit var upcomingAdapter: UpcomingAdapter


    companion object {
        @JvmStatic
        fun newInstance() = UpcomingFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_upcoming, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initializeViews()
    }

    override fun initializeViews() {
        initializeComponents()
    }

    override fun initializeComponents() {

        activity!!.title = getString(R.string.upcoming_movies)
        upcomingAdapter = UpcomingAdapter(context!!)
        recycler.adapter = upcomingAdapter
        chipGroupGenres.visibility = View.GONE

        refresh.setOnRefreshListener {
            upcomingViewModel.refresh()
        }

        initializeViewModel()
    }

    private fun initializeViewModel() {

        upcomingViewModel = ViewModelProviders.of(this).get(UpcomingViewModel::class.java)
        upcomingViewModel.getUpcomingMovies.observe(this, Observer { data ->
            upcomingAdapter.submitList(data)
            refresh.isRefreshing = false
        })

        upcomingViewModel.getGenres.observe(this, Observer { data ->

            chipGroupGenres.removeAllViews()
            if (context != null) {
                data.forEach { genre ->
                    val chip = ChipUtils.createChip(genre.name!!, true, context!!)
                    chip.id = genre.id.toInt()
                    chip.setOnCheckedChangeListener { buttonView, isChecked ->
                    }
                    chipGroupGenres.addView(chip)
                }
            }
            chipGroupGenres.setChipSpacing(10)
        })
    }

}
