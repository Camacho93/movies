package com.mvn.movies.userinterface.detailmovie

import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.palette.graphics.Palette
import com.mvn.movies.R
import com.mvn.movies.network.constants.ServiceConstants
import com.mvn.movies.userinterface.base.BaseActivity
import com.mvn.movies.utils.ThemeColorUtils
import com.mvn.movies.model.DetailMovieViewModel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.activity_detail_movie.*
import kotlinx.android.synthetic.main.content_detail_movie.*
import com.google.android.material.appbar.AppBarLayout
import com.mvn.movies.utils.ChipUtils
import com.mvn.movies.utils.MovieTypeUtils
import kotlinx.android.synthetic.main.content_detail_movie.chipGroupGenres
import kotlin.math.abs


class DetailMovie : BaseActivity() {

    /*VIEW MODELS*/
    private lateinit var detailMovieViewModel: DetailMovieViewModel

    companion object {
        @JvmStatic
        fun newInstance() = DetailMovie()
        const val MOVIE_ID: String = "MOVIE_ID"

        const val MOVIE_TYPE: String = "MOVIE_TYPE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        initializeViews()
    }

    override fun initializeViews() {

        initializeComponents()
    }

    override fun initializeComponents() {

        appBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                txtTitle.visibility= View.GONE

            } else {
                txtTitle.visibility= View.VISIBLE
            }
        })
        initializeViewModel()
    }

    private fun initializeViewModel() {

        detailMovieViewModel = ViewModelProviders.of(this).get(DetailMovieViewModel::class.java)

        when (intent.getIntExtra(MOVIE_TYPE, 0)) {

            MovieTypeUtils.popular -> {
                popularMovie()
            }
            MovieTypeUtils.topRated -> {
                topRatedMovie()
            }
            MovieTypeUtils.upcoming -> {
                upcomingMovie()
            }
        }

        detailMovieViewModel.getGenres(intent.getLongExtra(MOVIE_ID, 0)).observe(this, Observer { genres ->

            chipGroupGenres.removeAllViews()
            genres.forEach{ genre ->
                chipGroupGenres.addView(ChipUtils.createChip(genre.name!!, this))
            }

            chipGroupGenres.setChipSpacing(10)

        })
    }


    private fun setUpToolbar(url: String) {
        Picasso.get()
                .load(url)
                .placeholder(getDrawable(R.drawable.ic_movies))
                .error(getDrawable(R.drawable.ic_movies))
                .into(picassoTarget)
    }



    private val picassoTarget : Target = object : Target {
        override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
            Palette.from(bitmap).generate { palette ->

                val finalColor = ThemeColorUtils.getColor(palette, 0)
                    window.statusBarColor = finalColor

                val drawable: LayerDrawable = ratingBar.progressDrawable as LayerDrawable
                drawable.setColorFilter(finalColor, PorterDuff.Mode.SRC_IN)

                collapsing.setContentScrimColor(finalColor)
                imageView.setImageBitmap(bitmap)
            }
        }

        override fun onBitmapFailed(e: Exception, errorDrawable: Drawable) {
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable) {
            imageView.setImageDrawable(placeHolderDrawable)
        }
    }


    private fun popularMovie(){

        detailMovieViewModel.getPopularMovieDetails(intent.getLongExtra(MOVIE_ID, 0)).observe(this, Observer { data ->
            if(data != null) {

                setUpToolbar(ServiceConstants.getPosterPath720(data.poster_path!!))

                txtTitle.text = data.title
                txtRelease.text = String.format(getString(R.string.release_date),  data.releaseDate)
                txtContent.text = data.overview
                ratingBar.rating = (data.average / 2).toFloat()
                collapsing.title = data.title

            }
        })

    }

    private fun topRatedMovie(){
        detailMovieViewModel.getTopRatedMovieDetails(intent.getLongExtra(MOVIE_ID, 0)).observe(this, Observer { data ->
            if(data != null) {

                setUpToolbar(ServiceConstants.getPosterPath720(data.poster_path!!))

                txtTitle.text = data.title
                txtRelease.text = String.format(getString(R.string.release_date),  data.releaseDate)
                txtContent.text = data.overview
                ratingBar.rating = (data.average / 2).toFloat()
                collapsing.title = data.title

            }
        })
    }
    private fun upcomingMovie(){

        detailMovieViewModel.getUpcomingMovieDetails(intent.getLongExtra(MOVIE_ID, 0)).observe(this, Observer { data ->
            if(data != null) {

                setUpToolbar(ServiceConstants.getPosterPath720(data.poster_path!!))

                txtTitle.text = data.title
                txtRelease.text = String.format(getString(R.string.release_date),  data.releaseDate)
                txtContent.text = data.overview
                ratingBar.rating = (data.average / 2).toFloat()
                collapsing.title = data.title

            }
        })
    }



}
