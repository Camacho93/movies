package com.mvn.movies.userinterface

import kotlinx.android.synthetic.main.activity_main_navigation.*

import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.facebook.stetho.Stetho
import com.mvn.movies.R
import com.mvn.movies.userinterface.base.BaseActivity

class MainNavigationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_navigation)

        Stetho.initializeWithDefaults(this)

        initializeNavigationUI()
        initializeViews()
    }

    override fun initializeViews() {
        initializeComponents()
    }

    override fun initializeComponents() {
        setSupportActionBar(toolbar)

    }

    private fun initializeNavigationUI() {
        val navController = Navigation.findNavController(this, R.id.fragment)
        NavigationUI.setupWithNavController(bottomNavigationView, navController)
        bottomNavigationView.setupWithNavController(navController)
    }
}
