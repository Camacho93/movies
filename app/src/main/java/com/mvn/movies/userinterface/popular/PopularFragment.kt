package com.mvn.movies.userinterface.popular

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mvn.movies.R
import com.mvn.movies.presenter.adapter.PopularAdapter
import com.mvn.movies.userinterface.base.BaseFragment
import com.mvn.movies.utils.ChipUtils
import com.mvn.movies.model.PopularViewModel
import kotlinx.android.synthetic.main.fragment_popular.*


class PopularFragment : BaseFragment() {

    /*VIEW MODELS*/
    private lateinit var popularViewModel: PopularViewModel

    /*VARIABLE*/
    private lateinit var popularAdapter: PopularAdapter

    companion object {
        @JvmStatic
        fun newInstance() = PopularFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_popular, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initializeViews()
    }

    override fun initializeViews() {
        initializeComponents()
    }

    override fun initializeComponents() {

        activity!!.title = getString(R.string.popular_movies)
        popularAdapter = PopularAdapter(context!!)
        rvPopular.adapter = popularAdapter
        chipGroupGenres.visibility = View.GONE

        refresh.setOnRefreshListener {
            popularViewModel.refresh()
        }

        initializeViewModel()
    }

    private fun initializeViewModel() {

        popularViewModel = ViewModelProviders.of(this).get(PopularViewModel::class.java)
        popularViewModel.getPopularMovies.observe(this, Observer { data ->
            popularAdapter.submitList(data)
            refresh.isRefreshing = false
        })

        popularViewModel.getGenres.observe(this, Observer { data ->

            chipGroupGenres.removeAllViews()
            if (context != null) {
                data.forEach { genre ->
                    val chip = ChipUtils.createChip(genre.name!!, true, context!!)
                    chip.id = genre.id.toInt()
                    chip.setOnCheckedChangeListener { buttonView, isChecked ->
                    }
                    chipGroupGenres.addView(chip)
                }
            }
            chipGroupGenres.setChipSpacing(10)
        })
    }

}