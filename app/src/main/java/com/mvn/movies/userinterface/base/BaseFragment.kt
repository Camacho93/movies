package com.mvn.movies.userinterface.base

import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    abstract fun initializeViews()
    abstract fun initializeComponents()
}