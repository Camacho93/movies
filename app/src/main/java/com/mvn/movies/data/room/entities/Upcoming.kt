package com.mvn.movies.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "upcoming", primaryKeys = ["id"])
class Upcoming {
    @ColumnInfo(name = "id")
    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Long = 0

    @ColumnInfo(name = "title")
    @JsonProperty("title")
    @get:JsonProperty("title")
    @set:JsonProperty("title")
    var title: String? = null

    @ColumnInfo(name = "overview")
    @JsonProperty("overview")
    @get:JsonProperty("overview")
    @set:JsonProperty("overview")
    var overview: String? = null

    @ColumnInfo(name = "poster_path")
    @JsonProperty("poster_path")
    @get:JsonProperty("poster_path")
    @set:JsonProperty("poster_path")
    var poster_path: String? = null

    @ColumnInfo(name = "video")
    @JsonProperty("video")
    @get:JsonProperty("video")
    @set:JsonProperty("video")
    var hasVideo: Boolean = false

    @ColumnInfo(name = "release_date")
    @JsonProperty("release_date")
    @get:JsonProperty("release_date")
    @set:JsonProperty("release_date")
    var releaseDate: String = ""

    @ColumnInfo(name = "popularity")
    @JsonProperty("popularity")
    @get:JsonProperty("popularity")
    @set:JsonProperty("popularity")
    var popularity: Double = 0.0

    @ColumnInfo(name = "vote_average")
    @JsonProperty("vote_average")
    @get:JsonProperty("vote_average")
    @set:JsonProperty("vote_average")
    var average: Double = 0.0

    @ColumnInfo(name = "genre_ids")
    @JsonProperty("genre_ids")
    @get:JsonProperty("genre_ids")
    @set:JsonProperty("genre_ids")
    @Ignore
    var genres: List<Long> = ArrayList()
}