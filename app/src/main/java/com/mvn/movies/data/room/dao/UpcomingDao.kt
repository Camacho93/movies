package com.mvn.movies.data.room.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.mvn.movies.data.room.entities.Upcoming

@Dao
interface UpcomingDao {

    @Query("SELECT * FROM upcoming ORDER BY popularity DESC")
    fun getAllUpcomingMovies(): DataSource.Factory<Int, Upcoming>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertUpcomingMovie(upcoming: Upcoming):Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateUpcomingMovie(vararg upcoming: Upcoming)

    @Query("DELETE FROM upcoming")
    fun deleteAllUpcomingMovies()

    @Query("SELECT * FROM upcoming WHERE id = :id")
    fun getUpcomingMovie(id: Long): LiveData<Upcoming>

    @Query("SELECT p.* FROM upcoming p INNER JOIN upcoming_genre g ON p.id = g.upcomingId WHERE g.genreId IN (:filterValues) ORDER BY p.popularity DESC")
    fun getAllUpcomingMoviesByGenre(filterValues: List<Int>): DataSource.Factory<Int, Upcoming>


}