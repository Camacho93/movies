package com.mvn.movies.data.room.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.mvn.movies.data.room.entities.TopRated

@Dao
interface TopRatedDao {

    @Query("SELECT * FROM topRated ORDER BY vote_average DESC")
    fun getAllTopRatedMovies(): DataSource.Factory<Int, TopRated>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTopRatedMovie(topRated: TopRated):Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateTopRatedMovie(vararg topRated: TopRated)

    @Query("DELETE FROM topRated")
    fun deleteAllTopRatedMovies()

    @Query("SELECT * FROM topRated WHERE id = :id")
    fun getTopRatedMovie(id: Long): LiveData<TopRated>

    @Query("SELECT p.* FROM topRated p INNER JOIN top_rated_genre g ON p.id = g.topRatedId WHERE g.genreId IN (:filterValues) ORDER BY p.vote_average DESC")
    fun getAllTopRatedMoviesByGenre(filterValues: List<Int>): DataSource.Factory<Int, TopRated>


}