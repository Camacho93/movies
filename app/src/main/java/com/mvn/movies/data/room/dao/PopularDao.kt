package com.mvn.movies.data.room.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.mvn.movies.data.room.entities.Popular

@Dao
interface PopularDao {

    @Query("SELECT * FROM populars ORDER BY popularity DESC")
    fun getAllPopularsMovies(): DataSource.Factory<Int, Popular>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPopularMovie(popular: Popular):Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updatePopularMovie(vararg popular: Popular)

    @Query("DELETE FROM populars")
    fun deleteAllPopularMovies()


    @Query("SELECT * FROM populars WHERE id = :id")
    fun getPopularMovie(id: Long): LiveData<Popular>

    @Query("SELECT p.* FROM populars p INNER JOIN popular_genre g ON p.id = g.popularId WHERE g.genreId IN (:filterValues) ORDER BY p.popularity DESC")
    fun getAllPopularMoviesByGenre(filterValues: List<Int>): DataSource.Factory<Int, Popular>


}