package com.mvn.movies.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mvn.movies.data.room.dao.*
import com.mvn.movies.data.room.entities.*


@Database(entities = [Popular::class, PopularGenre::class, Genre::class, TopRated::class, TopRatedGenre::class,
    Upcoming::class, UpcomingGenre::class],
    version = 1,
    exportSchema = false)
abstract class MoviesDatabase : RoomDatabase() {

    companion object {
        @Volatile
        private var INSTANCE: MoviesDatabase? = null

        fun getDatabase(context: Context): MoviesDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MoviesDatabase::class.java,
                    "movies.db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

    abstract fun popularDao(): PopularDao

    abstract fun genreDao(): GenreDao

    abstract fun popularGenreDao(): PopularGenreDao

    abstract fun topRatedDao(): TopRatedDao

    abstract fun topRatedGenreDao(): TopRatedGenreDao

    abstract fun upcomingDao(): UpcomingDao

    abstract fun upcomingGenreDao(): UpcomingGenreDao
}