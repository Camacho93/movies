package com.mvn.movies.data.room.dao

import androidx.room.*
import com.mvn.movies.data.room.entities.PopularGenre

@Dao
interface PopularGenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPopularGenre(genre: PopularGenre): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updatePopularGenre(vararg genre: PopularGenre)
}