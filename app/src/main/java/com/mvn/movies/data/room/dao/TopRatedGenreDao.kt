package com.mvn.movies.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import com.mvn.movies.data.room.entities.TopRatedGenre

@Dao
interface TopRatedGenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTopRatedGenre(genre: TopRatedGenre): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateTopRatedGenre(vararg genre: TopRatedGenre)
}