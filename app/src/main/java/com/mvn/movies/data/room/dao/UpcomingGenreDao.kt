package com.mvn.movies.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import com.mvn.movies.data.room.entities.UpcomingGenre

@Dao
interface UpcomingGenreDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUpcomingGenre(genre: UpcomingGenre): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUpcomingGenre(vararg genre: UpcomingGenre)
}