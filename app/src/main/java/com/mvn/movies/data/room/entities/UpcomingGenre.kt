package com.mvn.movies.data.room.entities

import androidx.room.*
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "upcoming_genre",
    indices = [Index("upcomingId")],
    foreignKeys = [ForeignKey(entity =  Upcoming::class,
        parentColumns = ["id"],
        childColumns = ["upcomingId"],
        onDelete = ForeignKey.CASCADE)])
class UpcomingGenre (genreId: Long, upcomingId: Long) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Long = 0

    @ColumnInfo(name = "genreId")
    @JsonProperty("genreId")
    @get:JsonProperty("genreId")
    @set:JsonProperty("genreId")
    var genreId: Long = genreId

    @ColumnInfo(name = "upcomingId")
    @JsonProperty("upcomingId")
    @get:JsonProperty("upcomingId")
    @set:JsonProperty("upcomingId")
    var upcomingId: Long = upcomingId

}