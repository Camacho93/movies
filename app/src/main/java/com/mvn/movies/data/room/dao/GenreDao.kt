package com.mvn.movies.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mvn.movies.data.room.entities.Genre

@Dao
interface GenreDao {


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGenre(genre: Genre): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateGenre(vararg genre: Genre)

    @Query("SELECT * FROM genre")
    fun getAllGenre(): LiveData<List<Genre>>


    @Query("SELECT g.* FROM genre g INNER JOIN popular_genre pg ON pg.genreId = g.id WHERE pg.popularId = :popularMovieId")
    fun getAllGenreByPopularId(popularMovieId: Long): LiveData<List<Genre>>
}