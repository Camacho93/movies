package com.mvn.movies.data.room.entities

import androidx.room.*
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "top_rated_genre",
    indices = [Index("topRatedId")],
    foreignKeys = [ForeignKey(entity =  TopRated::class,
        parentColumns = ["id"],
        childColumns = ["topRatedId"],
        onDelete = ForeignKey.CASCADE)])
class TopRatedGenre (genreId: Long, topRatedId: Long) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @JsonProperty("id")
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    var id: Long = 0

    @ColumnInfo(name = "genreId")
    @JsonProperty("genreId")
    @get:JsonProperty("genreId")
    @set:JsonProperty("genreId")
    var genreId: Long = genreId

    @ColumnInfo(name = "topRatedId")
    @JsonProperty("topRatedId")
    @get:JsonProperty("topRatedId")
    @set:JsonProperty("topRatedId")
    var topRatedId: Long = topRatedId

}