package com.mvn.movies.data.room.entities

import androidx.room.*
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import androidx.annotation.NonNull




@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "popular_genre",
        indices = [Index("popularId")],
        foreignKeys = [ForeignKey(entity =  Popular::class,
                parentColumns = ["id"],
                childColumns = ["popularId"],
                onDelete = ForeignKey.CASCADE)])
class PopularGenre(genreId: Long, popularId: Long) {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        @JsonProperty("id")
        @get:JsonProperty("id")
        @set:JsonProperty("id")
        var id: Long = 0

        @ColumnInfo(name = "genreId")
        @JsonProperty("genreId")
        @get:JsonProperty("genreId")
        @set:JsonProperty("genreId")
        var genreId: Long = genreId

        @ColumnInfo(name = "popularId")
        @JsonProperty("popularId")
        @get:JsonProperty("popularId")
        @set:JsonProperty("popularId")
        var popularId: Long = popularId

}