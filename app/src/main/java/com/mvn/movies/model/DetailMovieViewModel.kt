package com.mvn.movies.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.mvn.movies.data.room.entities.Genre
import com.mvn.movies.data.room.entities.Popular
import com.mvn.movies.data.room.entities.TopRated
import com.mvn.movies.data.room.entities.Upcoming
import com.mvn.movies.repository.GenreRepository
import com.mvn.movies.repository.MoviesRepository

class DetailMovieViewModel (application: Application) : AndroidViewModel(application) {

    private var moviesRepository: MoviesRepository = MoviesRepository(application)
    private var genreRepository: GenreRepository = GenreRepository(application)


    fun getPopularMovieDetails(movieId: Long): LiveData<Popular> {
        return moviesRepository.getPopularMovie(movieId)
    }

    fun getTopRatedMovieDetails(movieId: Long): LiveData<TopRated> {
        return moviesRepository.getTopRateMovie(movieId)
    }

    fun getUpcomingMovieDetails(movieId: Long): LiveData<Upcoming> {
        return moviesRepository.getUpcomingMovie(movieId)
    }

    fun getGenres(filterValues: Long): LiveData<List<Genre>> {
        return  genreRepository.getGenresByPopularId(filterValues)
    }


}