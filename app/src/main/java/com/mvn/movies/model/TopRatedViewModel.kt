package com.mvn.movies.model

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mvn.movies.data.room.entities.Genre
import com.mvn.movies.data.room.entities.TopRated
import com.mvn.movies.presenter.boundary.TopRatedBoundaryCallback
import com.mvn.movies.repository.GenreRepository
import com.mvn.movies.repository.MoviesRepository
import com.mvn.movies.utils.PageUtils

class TopRatedViewModel (application: Application) : AndroidViewModel(application) {

    private var moviesRepository: MoviesRepository = MoviesRepository(application)
    private var genreRepository: GenreRepository = GenreRepository(application)
    private var dataSource: DataSource.Factory<Int, TopRated>
    private var app: Application

    private val connectivityNetwork = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    init {
        dataSource = moviesRepository.getAllTopRatedMovies()
        app = application
    }


    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .setPageSize(TopRatedBoundaryCallback.PAGE_SIZE)
        .build()

    val getTopRatesMovies = LivePagedListBuilder(dataSource, pagedListConfig)
        .setBoundaryCallback(
            TopRatedBoundaryCallback(application)
        )
        .build()


    fun getTopRatesMoviesByFilters(filterValues: List<Int>): LiveData<PagedList<TopRated>> {

        if (filterValues.isNotEmpty()) {
            return LivePagedListBuilder(moviesRepository.getTopRatedMoviesByGenre(filterValues), pagedListConfig)
                .setBoundaryCallback(
                    TopRatedBoundaryCallback(app)
                )
                .build()
        } else {
            return getTopRatesMovies
        }
    }


    val getGenres: LiveData<List<Genre>> = genreRepository.getAllGenres()

    fun refresh() {
        val activeNetwork = connectivityNetwork.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected) {
            AsyncTask.execute {
                PageUtils.popularPage = 0
                moviesRepository.deleteAllTopRatesMovies()
            }
        } else {
            getTopRatesMovies.value!!.dataSource.invalidate()
        }
    }
}