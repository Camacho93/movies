package com.mvn.movies.model

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mvn.movies.data.room.entities.Genre
import com.mvn.movies.data.room.entities.Upcoming
import com.mvn.movies.presenter.boundary.UpcomingBoundaryCallback
import com.mvn.movies.repository.GenreRepository
import com.mvn.movies.repository.MoviesRepository
import com.mvn.movies.utils.PageUtils

class UpcomingViewModel (application: Application) : AndroidViewModel(application) {

    private var moviesRepository: MoviesRepository = MoviesRepository(application)
    private var genreRepository: GenreRepository = GenreRepository(application)
    private var dataSource: DataSource.Factory<Int, Upcoming>
    private var app: Application

    private val connectivityNetwork = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    init {
        dataSource = moviesRepository.getAllUpcomingMovies()
        app = application
    }


    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .setPageSize(UpcomingBoundaryCallback.PAGE_SIZE)
        .build()

    val getUpcomingMovies = LivePagedListBuilder(dataSource, pagedListConfig)
        .setBoundaryCallback(
            UpcomingBoundaryCallback(application)
        )
        .build()


    fun getUpcomingMoviesByFilters(filterValues: List<Int>): LiveData<PagedList<Upcoming>> {

        if (filterValues.isNotEmpty()) {
            return LivePagedListBuilder(moviesRepository.getUpcomingMoviesByGenre(filterValues), pagedListConfig)
                .setBoundaryCallback(
                    UpcomingBoundaryCallback(app)
                )
                .build()
        } else {
            return getUpcomingMovies
        }
    }


    val getGenres: LiveData<List<Genre>> = genreRepository.getAllGenres()

    fun refresh() {
        val activeNetwork = connectivityNetwork.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected) {
            AsyncTask.execute {
                PageUtils.popularPage = 0
                moviesRepository.deleteAllTopRatesMovies()
            }
        } else {
            getUpcomingMovies.value!!.dataSource.invalidate()
        }
    }
}