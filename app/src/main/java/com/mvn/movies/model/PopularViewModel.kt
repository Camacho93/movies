package com.mvn.movies.model

import android.app.Application
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mvn.movies.data.room.entities.Popular
import com.mvn.movies.presenter.boundary.PopularBoundaryCallback
import com.mvn.movies.repository.MoviesRepository
import android.content.Context.CONNECTIVITY_SERVICE
import com.mvn.movies.data.room.entities.Genre
import com.mvn.movies.repository.GenreRepository
import com.mvn.movies.utils.PageUtils

class PopularViewModel(application: Application) : AndroidViewModel(application) {

    private var moviesRepository: MoviesRepository = MoviesRepository(application)
    private var genreRepository: GenreRepository = GenreRepository(application)
    private var  dataSourcePopularMovies: DataSource.Factory<Int, Popular>
    private lateinit var app: Application

    private val connectivityNetwork = application.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager

    init {
        dataSourcePopularMovies = moviesRepository.getAllPopularMovies()
        app = application
    }


    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .setPageSize(PopularBoundaryCallback.PAGE_SIZE)
        .build()

    val getPopularMovies = LivePagedListBuilder(dataSourcePopularMovies, pagedListConfig)
        .setBoundaryCallback(
            PopularBoundaryCallback(application))
        .build()


    fun getPopularMoviesByFilters(filterValues: List<Int>):LiveData<PagedList<Popular>> {

        if (filterValues.isNotEmpty()) {
            return LivePagedListBuilder(moviesRepository.getPopularMoviesByGenre(filterValues), pagedListConfig)
                .setBoundaryCallback(
                    PopularBoundaryCallback(app)
                )
                .build()
        }else{
            return getPopularMovies
        }
    }



    val getGenres: LiveData<List<Genre>> = genreRepository.getAllGenres()

    fun refresh() {
        val activeNetwork = connectivityNetwork.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected) {
            AsyncTask.execute {
                PageUtils.popularPage = 0
                moviesRepository.deleteAllPopularsMovies()
            }
        } else {
            getPopularMovies.value!!.dataSource.invalidate()
        }
    }
}