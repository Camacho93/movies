package com.mvn.movies.presenter.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mvn.movies.R
import com.mvn.movies.data.room.entities.TopRated
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.mvn.movies.presenter.viewholder.TopRatedViewHolder


class TopRatedAdapter (private val context: Context) : PagedListAdapter<TopRated, TopRatedViewHolder>(CALLBACK){

    companion object {

        private val CALLBACK = object : DiffUtil.ItemCallback<TopRated>() {
            override fun areItemsTheSame(oldItem: TopRated, newItem: TopRated): Boolean {
                return (oldItem.id == newItem.id )
            }

            override fun areContentsTheSame(oldItem: TopRated, newItem: TopRated): Boolean {
                return (oldItem.title == newItem.title
                        && oldItem.overview == newItem.overview)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopRatedViewHolder {
        val view : View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_popular_movie, parent, false)

        return TopRatedViewHolder(view, context)
    }

    override fun onBindViewHolder(holder: TopRatedViewHolder, position: Int) {
        if( getItem(position) != null)
            holder.bind(getItem(position)!!)
    }

}