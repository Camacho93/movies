package com.mvn.movies.presenter.boundary

import android.app.Application
import androidx.paging.PagedList
import com.mvn.movies.data.room.entities.TopRated
import com.mvn.movies.repository.MoviesRepository
import com.mvn.movies.utils.PageUtils

class TopRatedBoundaryCallback(application: Application) : PagedList.BoundaryCallback<TopRated>() {

    private val moviesRepository: MoviesRepository = MoviesRepository(application)

    companion object {
        const val PAGE_SIZE = 20
    }

    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        loadMovies()
    }

    override fun onItemAtEndLoaded(itemAtEnd: TopRated) {
        super.onItemAtEndLoaded(itemAtEnd)
        loadMovies()
    }

    private fun loadMovies() {

        PageUtils.topRatedPage += 1
        moviesRepository.getTopRates(PageUtils.topRatedPage)
    }
}
