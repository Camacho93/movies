package com.mvn.movies.presenter.boundary

import android.app.Application
import androidx.paging.PagedList
import com.mvn.movies.data.room.entities.Upcoming
import com.mvn.movies.repository.MoviesRepository
import com.mvn.movies.utils.PageUtils

class UpcomingBoundaryCallback(application: Application) : PagedList.BoundaryCallback<Upcoming>() {

    private val moviesRepository: MoviesRepository = MoviesRepository(application)

    companion object {
        const val PAGE_SIZE = 20
    }

    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        loadMovies()
    }

    override fun onItemAtEndLoaded(itemAtEnd: Upcoming) {
        super.onItemAtEndLoaded(itemAtEnd)
        loadMovies()
    }

    private fun loadMovies() {

        PageUtils.upcomingPage += 1
        moviesRepository.getUpcoming(PageUtils.upcomingPage)
    }
}
