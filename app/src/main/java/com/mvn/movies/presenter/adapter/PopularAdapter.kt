package com.mvn.movies.presenter.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mvn.movies.R
import com.mvn.movies.data.room.entities.Popular
import com.mvn.movies.presenter.viewholder.PopularViewHolder
import kotlin.collections.ArrayList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil


class PopularAdapter (private val context: Context) : PagedListAdapter<Popular, PopularViewHolder>(CALLBACK){

    companion object {

        private val CALLBACK = object : DiffUtil.ItemCallback<Popular>() {
            override fun areItemsTheSame(oldItem: Popular, newItem: Popular): Boolean {
                return (oldItem.id == newItem.id )
            }

            override fun areContentsTheSame(oldItem: Popular, newItem: Popular): Boolean {
                return (oldItem.title == newItem.title
                        && oldItem.overview == newItem.overview)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularViewHolder {
        val view : View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_popular_movie, parent, false)

        return PopularViewHolder(view, context)
    }

    override fun onBindViewHolder(holder: PopularViewHolder, position: Int) {
        if( getItem(position) != null)
            holder.bind(getItem(position)!!)
    }

}