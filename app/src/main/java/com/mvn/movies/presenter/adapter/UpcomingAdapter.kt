package com.mvn.movies.presenter.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mvn.movies.R
import com.mvn.movies.data.room.entities.Upcoming
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.mvn.movies.presenter.viewholder.UpcomingViewHolder


class UpcomingAdapter (private val context: Context) : PagedListAdapter<Upcoming, UpcomingViewHolder>(CALLBACK){

    companion object {

        private val CALLBACK = object : DiffUtil.ItemCallback<Upcoming>() {
            override fun areItemsTheSame(oldItem: Upcoming, newItem: Upcoming): Boolean {
                return (oldItem.id == newItem.id )
            }

            override fun areContentsTheSame(oldItem: Upcoming, newItem: Upcoming): Boolean {
                return (oldItem.title == newItem.title
                        && oldItem.overview == newItem.overview)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpcomingViewHolder {
        val view : View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_popular_movie, parent, false)

        return UpcomingViewHolder(view, context)
    }

    override fun onBindViewHolder(holder: UpcomingViewHolder, position: Int) {
        if( getItem(position) != null)
            holder.bind(getItem(position)!!)
    }

}