package com.mvn.movies.presenter.viewholder

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mvn.movies.R
import com.mvn.movies.data.room.entities.TopRated
import com.mvn.movies.network.constants.ServiceConstants
import com.mvn.movies.userinterface.detailmovie.DetailMovie
import com.mvn.movies.utils.MovieTypeUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_popular_movie.view.*

class TopRatedViewHolder(itemView: View, private val context: Context): RecyclerView.ViewHolder(itemView) {


    fun bind(topRated: TopRated){
        itemView.txtTitle.text =  topRated.title
        itemView.txtContent.text = topRated.overview

        itemView.setOnClickListener{
            val intent = Intent(context, DetailMovie::class.java)
            intent.putExtra(DetailMovie.MOVIE_ID, topRated.id)
            intent.putExtra(DetailMovie.MOVIE_TYPE, MovieTypeUtils.topRated)
            context.startActivity(intent)
        }

        Picasso.get()
            .load(ServiceConstants.getPosterPath(topRated.poster_path!!))
            .placeholder(context.getDrawable(R.drawable.ic_movies))
            .error(context.getDrawable(R.drawable.ic_movies))
            .into(itemView.imgView)

    }
}