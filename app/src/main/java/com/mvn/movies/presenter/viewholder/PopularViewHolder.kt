package com.mvn.movies.presenter.viewholder

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mvn.movies.R
import com.mvn.movies.data.room.entities.Popular
import com.mvn.movies.network.constants.ServiceConstants
import com.mvn.movies.userinterface.detailmovie.DetailMovie
import com.mvn.movies.utils.MovieTypeUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_popular_movie.view.*
import kotlinx.android.synthetic.main.item_popular_movie.view.txtContent

class PopularViewHolder (itemView: View, private val context: Context): RecyclerView.ViewHolder(itemView) {


    fun bind(popular: Popular){
        itemView.txtTitle.text =  popular.title
        itemView.txtContent.text = popular.overview

        itemView.setOnClickListener{
            val intent = Intent(context, DetailMovie::class.java)
            intent.putExtra(DetailMovie.MOVIE_ID, popular.id)
            intent.putExtra(DetailMovie.MOVIE_TYPE, MovieTypeUtils.popular)

            context.startActivity(intent)
        }

        Picasso.get()
            .load(ServiceConstants.getPosterPath(popular.poster_path!!))
            .placeholder(context.getDrawable(R.drawable.ic_movies))
            .error(context.getDrawable(R.drawable.ic_movies))
            .into(itemView.imgView)

    }
}