package com.mvn.movies.presenter.boundary

import android.app.Application
import androidx.paging.PagedList
import com.mvn.movies.data.room.entities.Popular
import com.mvn.movies.repository.MoviesRepository
import com.mvn.movies.utils.PageUtils

class PopularBoundaryCallback (application: Application) : PagedList.BoundaryCallback<Popular>() {

    private val moviesRepository: MoviesRepository = MoviesRepository(application)

    companion object {
        const val PAGE_SIZE = 20
    }

    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        loadMovies()
    }

    override fun onItemAtEndLoaded(itemAtEnd: Popular) {
        super.onItemAtEndLoaded(itemAtEnd)
        loadMovies()
    }

    private fun loadMovies() {

        PageUtils.popularPage += 1
        moviesRepository.getPopulars(PageUtils.popularPage)
    }
}
