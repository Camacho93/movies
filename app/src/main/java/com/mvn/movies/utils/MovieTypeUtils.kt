package com.mvn.movies.utils

object MovieTypeUtils {
    val popular: Int = 1
    val topRated: Int = 2
    val upcoming: Int = 3
}