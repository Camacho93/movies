package com.mvn.movies.utils

object PageUtils {
    var popularPage: Int = 0
    var topRatedPage: Int = 0
    var upcomingPage: Int = 0
}