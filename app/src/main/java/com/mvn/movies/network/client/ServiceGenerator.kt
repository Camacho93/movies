package com.mvn.movies.network.client

import android.text.TextUtils
import android.util.Log
import com.mvn.movies.network.constants.ServiceConstants

import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

object ServiceGenerator {

    private var retrofit: Retrofit? = null
    private var builder: OkHttpClient.Builder? = null
    private var retrofitBuilder: Retrofit.Builder? = null

    init {

        builder = OkHttpClient.Builder()
        builder!!.connectTimeout(1, TimeUnit.MINUTES)
        builder!!.readTimeout(45, TimeUnit.SECONDS)
        builder!!.writeTimeout(45, TimeUnit.SECONDS)

        retrofitBuilder = Retrofit.Builder()
            .baseUrl(ServiceConstants.BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create())

    }

    fun <S> createService(serviceClass: Class<S>): S {
        return createService(serviceClass, null)
    }

    fun <S> createService(serviceClass: Class<S>, accessToken: String?): S {
        if (!TextUtils.isEmpty(accessToken)) {
            Log.d(ServiceGenerator::class.java.name, "Empty access token")
        }

        retrofitBuilder!!.client(builder!!.build())
        retrofit = retrofitBuilder!!.build()

        return retrofit!!.create(serviceClass)
    }

}