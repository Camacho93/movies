package com.mvn.movies.network.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.mvn.movies.data.room.entities.Genre


@JsonIgnoreProperties(ignoreUnknown = true)
class GenreResponse {

    @JsonProperty("genres")
    @get:JsonProperty("genres")
    @set:JsonProperty("genres")
    var genres: MutableList<Genre>? = null
}