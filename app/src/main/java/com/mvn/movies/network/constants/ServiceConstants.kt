package com.mvn.movies.network.constants

class ServiceConstants {

    companion object{
        const val BASE_URL = "https://api.themoviedb.org/"
        private const val BASE_POSTER_PATH = "https://image.tmdb.org/t/p/w400"
        private const val BASE_POSTER_PATH_720 = "https://image.tmdb.org/t/p/w780"

        const val API_KEY_VALUE = "07d70a2cdf3a933add9f7808ebb00119"

        fun getPosterPath(posterPath: String): String {
            return BASE_POSTER_PATH + posterPath
        }

        fun getPosterPath720(posterPath: String): String {
            return BASE_POSTER_PATH_720 + posterPath
        }


        /*ENDPOINTS*/
        const val GET_ALL_POPULAR = "3/movie/popular"
        const val GET_ALL_GENRE= "3/genre/movie/list"
        const val GET_ALL_TOP_RATED = "3/movie/top_rated"
        const val GET_ALL_UPCOMING = "3/movie/upcoming"

        /* QUERY VARIABLES*/
        const val API_KEY= "api_key"
        const val PAGE = "page"

    }

}