package com.mvn.movies.network.client

import com.mvn.movies.network.responses.GenreResponse
import com.mvn.movies.network.responses.PopularMoviesResponse
import com.mvn.movies.network.responses.TopRatedMoviesResponse
import com.mvn.movies.network.responses.UpcomingMoviesResponse
import com.mvn.movies.network.service.MoviesService
import retrofit2.Call

object RestClientMovies {

    private val moviesService: MoviesService =  ServiceGenerator.createService(MoviesService::class.java)

    fun getAllPopularMovies(apiKey: String, page: Int): Call<PopularMoviesResponse>{
        return moviesService.getAllPopular(apiKey, page)
    }

    fun getAllGenre(apiKey: String): Call<GenreResponse>{
        return moviesService.getAllGenre(apiKey)
    }

    fun getAllTopRatedMovies(apiKey: String, page: Int): Call<TopRatedMoviesResponse>{
        return moviesService.getAllTopRated(apiKey, page)
    }

    fun getAllUpcomingMovies(apiKey: String, page: Int): Call<UpcomingMoviesResponse>{
        return moviesService.getAllUpcoming(apiKey, page)
    }
}