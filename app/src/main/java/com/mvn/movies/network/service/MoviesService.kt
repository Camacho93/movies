package com.mvn.movies.network.service

import com.mvn.movies.network.responses.PopularMoviesResponse
import com.mvn.movies.network.constants.ServiceConstants
import com.mvn.movies.network.responses.GenreResponse
import com.mvn.movies.network.responses.TopRatedMoviesResponse
import com.mvn.movies.network.responses.UpcomingMoviesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesService {

    @GET(ServiceConstants.GET_ALL_POPULAR)
    fun getAllPopular(
        @Query(ServiceConstants.API_KEY) apiKey: String,
        @Query(ServiceConstants.PAGE) page: Int)
        : Call<PopularMoviesResponse>


    @GET(ServiceConstants.GET_ALL_GENRE)
    fun getAllGenre(
            @Query(ServiceConstants.API_KEY) apiKey: String)
            : Call<GenreResponse>

    @GET(ServiceConstants.GET_ALL_TOP_RATED)
    fun getAllTopRated(
        @Query(ServiceConstants.API_KEY) apiKey: String,
        @Query(ServiceConstants.PAGE) page: Int)
            : Call<TopRatedMoviesResponse>

    @GET(ServiceConstants.GET_ALL_UPCOMING)
    fun getAllUpcoming(
        @Query(ServiceConstants.API_KEY) apiKey: String,
        @Query(ServiceConstants.PAGE) page: Int)
            : Call<UpcomingMoviesResponse>
}