package com.mvn.movies.network.responses

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.mvn.movies.data.room.entities.Upcoming


@JsonIgnoreProperties(ignoreUnknown = true)
class UpcomingMoviesResponse {

    @JsonProperty("page")
    @get:JsonProperty("page")
    @set:JsonProperty("page")
    var page: Int = 0
    @JsonProperty("total_pages")
    @get:JsonProperty("total_pages")
    @set:JsonProperty("total_pages")
    var total_pages: Int = 0
    @JsonProperty("results")
    @get:JsonProperty("results")
    @set:JsonProperty("results")
    var results: MutableList<Upcoming>? = null
}